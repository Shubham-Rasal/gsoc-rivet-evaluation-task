import numpy as np


def invert_matrix(N):
    A = np.random.rand(N, N)
    print("Original matrix: \n", A)
    A_inv = np.linalg.inv(A)
    return A_inv


# take command line input while running the program
if __name__ == '__main__':
    import sys
    N = int(sys.argv[1])

    #check if N>0
    if N <= 0:
        print("N should be greater than 0")
        sys.exit(1)
    

    #time the function
    import time
    start = time.time()
    inverted_matrix = invert_matrix(N)
    end = time.time()
    print("Inverted matrix: \n", inverted_matrix)

    print("Time taken: ", end-start, "seconds")
    
