# GSoC Rivet Evaluation Task

## Python Program to find the inverse of a matrix

```python
import numpy as np


def invert_matrix(N):
    A = np.random.rand(N, N)
    print("Original matrix: \n", A)
    A_inv = np.linalg.inv(A)
    return A_inv


# take command line input while running the program
if __name__ == '__main__':
    import sys
    N = int(sys.argv[1])
    inverted_matrix = invert_matrix(N)
    print("Inverted matrix: \n", inverted_matrix)


```

This program takes in a command line argument N, and generates a random NxN matrix. It then calculates the inverse of the matrix using the numpy library and prints the original and inverted matrix.

To run the program, use the following command:

```bash

python inverse_matrix.py 3

```

where 3 is the size of the matrix. You can replace 3 with any positive integer.

### Docker

We have to package the program into a minimal docker container. After a little bit of research, I managed to create a Dockerfile build that is slimmer than the official Python alpine image.

The image has python3 and pip3 installed with a copy of the ` inverse_matrix.py` file.

![Image size comparison](image.png)

References:

- https://github.com/docker-library/python/blob/eba24df439d48988302a60cf9ef5cddd5d49b51f/3.12/alpine3.19/Dockerfile
- https://stackoverflow.com/questions/62554991/how-do-i-install-python-on-alpine-linux
- https://github.com/jfloff/alpine-python

### GitLab CI

The next step is to create a GitLab CI pipeline that builds the docker image and pushes it to the GitLab container registry. The pipeline should also run the docker container and test the output of the program.

There are two stages in the pipeline:

1. build - Build and push the docker image (scheduled to run every day at 12:00 AM)
2. run - Run the docker container and time the run

The assumption for splitting the pipeline into two stages is that the build stage needs less frequent runs than the run stage. The build stage is scheduled to run every day at 12:00 AM, while the run stage runs on every commit to the repository.

Here's how the pipeline looks like:

![Pipeline](image-1.png)

https://gitlab.com/Shubham-Rasal/gsoc-rivet-evaluation-task/-/pipelines/1211276922

### Build Stage

This stage uses the docker image along with the dind (Docker in Docker) service to build the docker image. The dind service allows us to run docker commands inside the docker container. After building and tagging the image, we push it to the GitLab container registry.

### Different Runners for `run program` stage (Linux and MacOS)

To test the pipeline on different runner, specifically Linux and MacOS, we need to modify the build stage to build the image for both architectures. This can be done with the help of buildx, a CLI plugin for docker that extends the docker build command to support multiple platforms.

The `run program` stage is split into two jobs, one for each runner.

```yaml
run_linux_amd64:
  stage: run program
  tags:
    - saas-linux-medium-amd64
  image: docker
  services:
    - docker:dind
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    - time docker run $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG 5000

run_macos_arm64:
  stage: run program
  tags:
    - saas-macos-medium-m1
  image: docker
  services:
    - docker:dind
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    - time docker run $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG 5000
```

Similar to the build stage, we use the dind service to run the docker container. The `run_linux_amd64` job runs on a Linux runner with an amd64 architecture, while the `run_macos_arm64` job runs on a MacOS runner with an arm64 architecture since the docker image is built for these two architectures.

Supported architectures for the docker image: amd64, arm32v6, arm32v7, arm64v8, windows-amd64

The run for N=5000 is timed two ways, one from the inside of the program and the other from the outside using the `time` command. This is done to examine the time taken by the program to run and the time taken by the docker container to start and stop.

For this [job](https://gitlab.com/Shubham-Rasal/gsoc-rivet-evaluation-task/-/jobs/6380043524), here is how much time it took on the Linux runner:

![Job Run Time](image-2.png) 

Note: Due to lack of access to a MacOS runner, I was unable to test the pipeline on MacOS. However, the pipeline should work as expected on a MacOS runner as the base image choosen by me ships multi-arch docker images which will be automatically picked by the docker engine.


## Build System for Multi-Arch Docker Images

Building multi-arch docker images can be done using buildx due to the following architecture:

![Docker Desktop Architecture](image-3.png)

This is shipped with Docker Desktop and can be used to build multi-arch images locally. To make it work in our CI pipeline we need to do some modifications that will be discussed later.


Our system will be based around `buildx` which is provided as a CLI plugin for docker.

### buildx 

The buildx command is a CLI plugin for docker that extends the docker build command to support multiple platforms. It is a great tool for building multi-arch images and is recommended by the official docker documentation.


## Flow of the System

![Flow Diagram](image-4.png)


### Steps

The system is wrapped into a thing and simple CLI utility that let's you configure your mode (local or ci), image tags, and required architectures.

1. The user runs the CLI utility and sets the mode to local or ci. The user also sets the image tag and the required architectures.
    - In case the user chooses the local mode, the system builds the multi-arch image locally and pushes it to the docker registry using the `buildx` provided by Docker Desktop.
    - In case the user chooses the ci mode, the system builds the multi-arch image in the CI pipeline and pushes it to the GitLab container registry.

2. The steps after choosing the mode are the same for both local and ci modes. 

 - Create a new builder configuration with the desired platforms and architectures.
 - Build the image using the builder configuration.
 - Push the image to the registry.

### Considerations in the Gitlab CI

#### Using `docker manifest`

Since Gitlab provides runner with different architectures, we could make use of multiple runners running on machines with diffrent HW architectures and combining them with the 
`manifest` feature provided by docker.

![Manifest System Design](image-5.png)

The CI pipeline will parallely build the images with their respective architectures and push them to the container registry.

In a later stage, the manifest command can be used to combine all the images in a single multi-arch image using the `manifest` command.


#### Using `buildx` in the CI pipeline

This method will be similar to the local build setup given that we use a base image with docker, dind, and buildx pre-installed in the Gitlab CI.

This would make the build HW independent due to the virutalisation provided by QEMU (with LinuxKit under the hood), which means it could be run on any type of runners.

References

- https://www.docker.com/blog/multi-arch-images/
- https://docs.gitlab.com/ee/ci/runners/index.html
- https://docs.docker.com/reference/cli/docker/manifest/
- https://developer.ibm.com/tutorials/build-multi-architecture-x86-and-power-container-images-using-gitlab/