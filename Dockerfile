FROM alpine:3.19


# Install the required packages
RUN apk add --no-cache python3 py3-pip
RUN apk add --no-cache py3-numpy


#Copy the invert_matrix.py file to the container
COPY invert_matrix.py /invert_matrix.py

# Run the python script with the arg passed to the container
ENTRYPOINT [ "python3", "/invert_matrix.py" ]

